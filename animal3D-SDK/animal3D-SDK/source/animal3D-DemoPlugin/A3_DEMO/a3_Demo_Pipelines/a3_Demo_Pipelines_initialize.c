/*
	Copyright 2011-2020 Daniel S. Buckstein

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

		http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
*/

/*
	animal3D SDK: Minimal 3D Animation Framework
	By Daniel S. Buckstein

	a3_Demo_Pipelines_initialize.c
	Demo mode implementations: pipelines initialization.

	********************************************
	*** INITIALIZATION FOR PIPELINES MODE    ***
	********************************************
*/

//-----------------------------------------------------------------------------

#include "../a3_Demo_Pipelines.h"

typedef struct a3_DemoState a3_DemoState;
//#include "../a3_DemoState.h"


//-----------------------------------------------------------------------------

void a3pipelines_init(a3_DemoState const* demoState, a3_Demo_Pipelines* demoMode)
{
	demoMode->pipeline = pipelines_forward;
	demoMode->render = pipelines_renderPhong;
	demoMode->display = pipelines_displayTexture;
	demoMode->activeCamera = pipelines_cameraSceneViewer;
	demoMode->pass = pipelines_passComposite;
	demoMode->target_shadow = pipelines_shadow_fragdepth;
	demoMode->target_scene = pipelines_scene_finalcolor;
	demoMode->target_composite = pipelines_composite_finalcolor;
}


//-----------------------------------------------------------------------------
